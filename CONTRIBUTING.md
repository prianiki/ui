## How to add a new component to prianik-ui

1. Decide on a component you'd like to add to prianik-ui
1. Determine and document the scenarios (different types of implementations, if any) in which this component is used in an issue in prianik-ui
1. Create a merge request (MR) in prianik-ui implementing your new component
   1. Be sure to name your MR `feat: <commit message>` as that is needed for our npm release CI job **(don't forget to set the MR to squash commit)**
   1. Create a directory and component.vue in `components/` directory
   1. Create a story `.stories.js` in the your component's directory
   1. Import your component to `index.ts`
