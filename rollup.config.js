import path from 'path'
import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import vue from 'rollup-plugin-vue'
import typescript from 'rollup-plugin-typescript2'
import alias from 'rollup-plugin-alias'
import { terser } from 'rollup-plugin-terser'
import bundleSize from 'rollup-plugin-filesize'
import css from 'rollup-plugin-css-porter'
import { DEFAULT_EXTENSIONS } from '@babel/core'

export default {
  external: [
    'date-fns',
    'date-fns/locale/en',
    'lodash',
    'lodash/maxBy',
    'lodash/orderBy',
    'lodash/slice',
    'numeral',
    'popper.js',
    'vue',
    'vue-class-component',
    'vue-click-outside',
    'vue-functional-data-merge',
    'vue-property-decorator',
    'vue-scrollto'
  ],
  input: './src/index.ts',
  output: {
    file: 'dist/ui.js',
    format: 'cjs',
  },
  plugins: [
    alias({
      resolve: ['.vue', '.ts', '.js'],
      '@': path.resolve('./src')
    }),
    resolve(),
    css({ output: true }),
    vue({
      css: false
    }),
    commonjs(),
    bundleSize(),
    terser(),
    typescript({
      typescript: require('typescript'),
      useTsconfigDeclarationDir: true,
      objectHashIgnoreUnknownHack: true
    }),
    babel({
      exclude: 'node_modules/**',
      extensions: [
        ...DEFAULT_EXTENSIONS,
        '.ts',
        '.tsx'
      ],
      runtimeHelpers: true
    })
  ]
}
