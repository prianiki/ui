module.exports = {
  moduleNameMapper: {
    '^(.*).svg(.*)$': '<rootDir>/svgTransform.js',
    '^@/(.*)$': '<rootDir>/src/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js'
  },
  moduleFileExtensions: ['js', 'vue', 'json', 'ts'],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    '^.+\\.jsx?$': 'babel-jest',
    '^.+\\.tsx?$': 'ts-jest'
  },
  testMatch: [
    '*/**/*.spec.ts'
  ],
  transformIgnorePatterns: [
    '<rootDir>/node_modules/'
  ]
}
