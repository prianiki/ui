# prianik-ui

`prianik-ui` is a is a component library written in [Vue.js](https://vuejs.org).  
It focuses only on functionality, the looks are on you.

# Quickstart
```
# Clone the project
git clone git@gitlab.com:prianiki/ui.git prianik-ui

# Navigate to the root of the project
cd prianik-ui

# Install all the dependencies of the project
npm i

# Build and launch storybook to see the components in the browser
npm run storybook
```

## Installation

```sh
npm i @prianiki/ui
```

## Contributing guide

Please refer to [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to add new components and contribute in general to `prianik-ui`.
