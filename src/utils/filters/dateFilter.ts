import { format, getDaysInMonth, setDate, setMonth, setYear, isBefore, isAfter } from 'date-fns'

export const dateFilter = (unformatted : string, minDate? : Date, maxDate? : Date) => {
  // prevents double slashing
  unformatted = unformatted.replace('//', '/')

  const dateArray = unformatted.split('/')
  let day = dateArray[0]
  let month = dateArray[1]
  let year = dateArray[2]

  let outputDate = new Date()
  // if the month is not set yet, use jan (has 31 days)
  outputDate = setMonth(outputDate, 0)

  // prevents from overwriting 2 digits days (write 1 > becomes 01)
  if (day.length !== 2 && !month && !unformatted.includes('/')) {
    return unformatted
  }

  const max = getDaysInMonth(outputDate)
  if (parseInt(day) > max) {
    day = String(max)
  }
  outputDate = setDate(outputDate, parseInt(day))

  if (!month) {
    if (unformatted.includes('/')) {
      return format(outputDate, 'DD') + '/'
    } else {
      return format(outputDate, 'DD')
    }
  }

  if (month.length !== 2 && !year) {
    // allows input month in one digit
    if (unformatted.substring(unformatted.length - 1) === '/') {
      month = String(parseInt(month) - 1)
      outputDate = setMonth(outputDate, parseInt(month))
      return format(outputDate, 'DD/MM') + '/'
    } else {
      return format(outputDate, 'DD/') + month
    }
  }

  if (month) {
    if (parseInt(month) > 12) {
      month = '12'
    }
  }
  month = String(parseInt(month) - 1)
  outputDate = setMonth(outputDate, parseInt(month))

  if (!year) {
    if (unformatted.substring(unformatted.length - 1) === '/') {
      return format(outputDate, 'DD/MM') + '/'
    } else {
      return format(outputDate, 'DD/MM')
    }
  }

  if (year.length < 4) {
    return format(outputDate, 'DD/MM/') + year
  }

  outputDate = setYear(outputDate, parseInt(year))

  if (minDate && isBefore(outputDate, minDate)) {
    outputDate = minDate
  }

  if (maxDate && isAfter(outputDate, maxDate)) {
    outputDate = maxDate
  }

  // years
  return format(outputDate, 'DD/MM/YYYY')
}
