export const timeFilter = (unformatted : string) => {
  unformatted = unformatted.replace('::', ':')
  let result : string[] = []

  let splitted : string[] = unformatted.split('-')

  splitted.forEach((time) => {
    let formattedTime : string[] = []
    const timeArray = time.split(':')
    let hours = timeArray[0]
    let minutes = timeArray[1]

    if (hours) {
      if (time.includes(':')) {
        formattedTime.push(`${checkHours(hours, time)}:`)
      } else {
        let checked = checkHours(hours, time)
        if (checked !== undefined) {
          formattedTime.push(checked)
        }
      }
    }

    if (minutes) {
      formattedTime.push(checkMinutes(minutes, time))
    }

    result.push(formattedTime.join(''))
  })
  return result.join('-')
}

const checkHours = (hours : string, raw : string) : string | undefined => {
  let formattedHours
  if (!hours) {
    return
  }
  if (hours.length < 2 && !raw.includes(':')) {
    formattedHours = hours
  } else {
    if (hours.length < 2 && raw.includes(':')) {
      formattedHours = `0${hours}`
    } else if (parseInt(hours) > 23) {
      formattedHours = '23'
    } else {
      formattedHours = hours
    }
  }
  return formattedHours
}

const checkMinutes = (minutes : string, raw : string) => {
  if (!minutes) {
    return
  }
  let formattedMinutes
  if (minutes.length < 2) {
    if (raw.substring(raw.length - 1) === ':') {
      if (parseInt(minutes) > 5) {
        formattedMinutes = '59'
      } else {
        formattedMinutes = `${minutes}0`
      }
    } else {
      formattedMinutes = minutes
    }
  }
  if (minutes.length === 2) {
    if (parseFloat(minutes) > 59) {
      formattedMinutes = '59'
    } else {
      formattedMinutes = minutes
    }
  }

  return formattedMinutes
}
