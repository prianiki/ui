import { dateFilter } from '../dateFilter'

describe('date filter for masked input', () => {
  it('should return formatted date', () => {
    expect(dateFilter('99/99/9999')).toEqual('31/12/9999')
  })

  it('should range max and min when arguments passed', () => {
    expect(dateFilter(
      '99/99/9999', new Date(), new Date(2020, 1,1)
    )).toEqual('01/02/2020')

    expect(dateFilter(
      '99/99/2010', new Date(2020, 1, 1)
    )).toEqual('01/02/2020')
  })
})
