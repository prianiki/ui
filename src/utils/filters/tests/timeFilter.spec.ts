import { timeFilter } from '../timeFilter'

describe('time filter for masked input', () => {
  it('should format time right', () => {
    expect(timeFilter('99:99-99:99')).toEqual('23:59-23:59')
  })
})
