import { startOfWeek, addDays, format, getISODay, startOfMonth, endOfMonth, subMonths, getDaysInMonth, addMonths } from 'date-fns'

export const createWeek = (dayFormat: string, locale: Object): string[] => {
  const start = addDays(startOfWeek(new Date()), 1)
  return Array.from(Array(7), (x, index) => {
    return format(addDays(start, index), dayFormat, { locale })
  })
}

export const createMonth = (month: Date): Date[] => {
  let startDayOfWeek = getISODay(startOfMonth(month))
  let endDayOfWeek = getISODay(endOfMonth(month))

  let days: Date[] = []

  if (startDayOfWeek !== 0) {
    let previousMonth = startOfMonth(subMonths(startOfMonth(month), 1))
    let previousMonthLength = getDaysInMonth(previousMonth)
    let overflow = startDayOfWeek - 1
    for (let i = previousMonthLength - overflow; i <= previousMonthLength; i++) {
      days.push(addDays(previousMonth, i))
    }
  }

  let monthLength = getDaysInMonth(month)
  for (let i = 1; i < monthLength; i++) {
    days.push(addDays(startOfMonth(month), i))
  }

  if (endDayOfWeek < 7) {
    const overflow = 7 - endDayOfWeek
    const nextMonth = startOfMonth(addMonths(startOfMonth(month), 1))
    for (let i = 0; i < overflow; i++) {
      days.push(addDays(nextMonth, i))
    }
  }

  return days
}
