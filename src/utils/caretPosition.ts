export const getCaretPosition = (el) => {
  if (!el) {
    return 0
  }

  try {
    let start = el.selectionStart
    let end = el.selectionEnd

    if (start === end) {
      return start
    } else {
      return end
    }
  } catch (e) {
    return 0
  }
}

export const setCaretPosition = (el, position) => {
  if (el && el.setSelectionRange) {
    el.setSelectionRange(position, position)
  }
}

export const keepCaretPosition = (el: HTMLInputElement, oldPosition: number, newValue: string, unmaskedValue: string, previousValue?: string) => {
  const oldLength = String(unmaskedValue).length
  const newLength = String(newValue).length
  let caretPosition

  if (previousValue != null && newValue === previousValue) {
    caretPosition = oldPosition
  } else {
    if (newLength <= oldLength) {
      if (oldLength - newLength > 1) {
        caretPosition = oldPosition - 1
      } else {
        if (String(newValue) === '0') {
          caretPosition = oldPosition + 1
        } else {
          caretPosition = oldPosition
        }
      }
    } else {
      if (newLength - oldLength >= 1) {
        caretPosition = oldPosition + 1
      } else {
        caretPosition = oldPosition
      }
    }
  }

  try {
    setCaretPosition(el, caretPosition)
  } catch (e) {}
}
