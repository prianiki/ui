export const isInEnum = (value, Enum) => {
  return Object.values(Enum).includes(value)
}

export * from './strings'
