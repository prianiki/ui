import latinize from 'latinize'
import _ from 'lodash'

/**
 * @deprecated use assertEmpty
 */
export const isStringEmpty = (value : any): boolean => {
  return assertEmpty(value)
}

export const assertEmpty = (value: any): boolean => {
  if (value == null) {
    return true
  }

  if (typeof value === 'string') {
    return String(value).trim().length === 0
  }

  return _.isEmpty(value)
}

export const ID = () => {
  return '_' + (Math.random() * Math.random()).toString(36).substr(2, 9)
}

/**
 * @deprecated use assertStringContainsLoose
 */
export const searchInString = (search, string) => {
  return assertStringContainsLoose(string, search)
}

export const assertStringContainsLoose = (haystack: string, needle: string) => {
  if (assertEmpty(haystack) || assertEmpty(needle)) {
    return false
  }

  return simplify(haystack).includes(simplify(needle))
}

export function simplify (string): string {
  return slugify(String(string).trim().toLowerCase(), {
    replacement: ' '
  })
}

/** Makes a slug of anything */
export function slugify (text: String, options: SlugifyOptions = {
  replacement: '-'
}): string {
  return latinize(String(text)
    .toLowerCase()).replace(/\s+/g, options.replacement) // Replace spaces with -
    .replace(/--+/g, options.replacement) // Replace multiple - with single -
    .replace(/[^\w-]+/g, '') // Remove all non-word chars
    .trim()
}

type SlugifyOptions = {
  replacement: string
}
