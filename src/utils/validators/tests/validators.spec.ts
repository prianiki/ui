import { email, length } from '../index'

describe('validators', () => {
  describe('email validator', () => {
    it('should process emails right', () => {
      expect(email('test@test.ru')).toBe(true)
      expect(email('test@test')).toBe(false)
    })
  })

  describe('string length validator', () => {
    it('should check strings length right', () => {
      let checker = length(5, 15)

      expect(checker('test')).toBe(false)
      expect(checker('test123')).toBe(true)
    })
  })
})
