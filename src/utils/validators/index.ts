import { email as emailRE } from './regularExpressions'

export const email = (value: string) : boolean => {
  return emailRE.test(String(value).trim().toLowerCase())
}

export const length = (minLength = 0, maxLength = 0) => {
  return (value: string) => {
    const valueLength = String(value).trim().length
    let isValid = true

    if (!valueLength) {
      return false
    }

    if (minLength) {
      isValid = valueLength >= minLength
    }

    if (maxLength) {
      isValid = isValid && (valueLength <= maxLength)
    }

    return isValid
  }
}
