import numeral from 'numeral'

numeral.register('locale', 'fr', {
  delimiters: {
    thousands: ' ',
    decimal: '.'
  },
  abbreviations: {
    thousand: 'k',
    million: 'm',
    billion: 'b',
    trillion: 't'
  },
  ordinal: function (number : number) {
    return number === 1 ? 'er' : 'ème'
  },
  currency: {
    symbol: '€'
  }
})

numeral.locale('fr')

export const formatNumeral = (value : any, format? : any) => {
  try {
    return numeral(value).format(format)
  } catch (e) {
    return value
  }
}

export const getNumeralValue = (value : any, format? : any) => {
  try {
    // @ts-ignore
    return numeral(value).value(format)
  } catch (e) {
    return value
  }
}
