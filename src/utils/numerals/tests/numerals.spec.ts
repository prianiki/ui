import { formatNumeral, getNumeralValue } from '@/utils/numerals'

describe('numeral methods', () => {
  it('should format numbers as expected and return value if passed a formatted', () => {
    expect(formatNumeral(1000)).toEqual('1 000')
    expect(getNumeralValue('1 000')).toEqual(1000)
  })
})
