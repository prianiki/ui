import { Vue, Prop, Watch, Component } from 'vue-property-decorator'
import {
  HeaderCellType,
  TableSortType,
  UnionBodyRowType
} from '@/components/PTable/PTableTypes'
import orderBy from 'lodash/orderBy'
import slice from 'lodash/slice'
import maxBy from 'lodash/maxBy'

@Component({})
export default class TableMixin extends Vue {
  @Prop({
    default () {
      return []
    } }) header : HeaderCellType[]
  @Prop({
    default () {
      return []
    }
  }) body : UnionBodyRowType[]
  @Prop({ default: 'created' }) defaultSortBy : string
  @Prop() showOnly : string | null
  @Prop() getMaxBy : string | null
  @Prop({ default: true }) manageSort : boolean

  sort : TableSortType = {
    sortBy: null,
    asc: false
  }

  @Watch('defaultSortBy', { immediate: true })
  sortHandler (value) {
    if (value) {
      this.sort.sortBy = value
    }
  }

  @Watch('maxValue', { immediate: true })
  maxValueHandler (value) {
    if (value) {
      this.$emit('maxValue', value)
    }
  }

  get bodyData () {
    const body = this.body
    const sort = this.sort
    const showOnly = this.showOnly

    if (!this.manageSort) {
      return this.body
    }

    let data = orderBy(body, (x : UnionBodyRowType) => {
      let cells

      if ('cells' in x) {
        cells = x.cells
      } else {
        cells = x
      }

      const filtered = cells.filter(y => y.sortKey === sort.sortBy)
      return filtered.length ? filtered[0].sortValue : null
    }, [sort.asc ? 'asc' : 'desc'])

    if (showOnly) {
      data = slice(data, 0, showOnly)
    }

    return data
  }

  get maxValue () {
    const getMaxBy = this.getMaxBy
    const bodyData = this.bodyData

    if (getMaxBy) {
      let item = maxBy(bodyData, (x : UnionBodyRowType) => {
        let cells

        if ('cells' in x) {
          cells = x.cells
        } else {
          cells = x
        }

        const filtered = cells.filter(y => y.sortKey === getMaxBy)
        return filtered.length ? filtered[0].sortValue : null
      })
      if (item) {
        if (item.cells) {
          item = item.cells
        }
        const cell = item.filter(x => x.sortKey === getMaxBy)
        if (cell && cell[0] && cell[0].sortValue) {
          return cell[0].sortValue
        }
      }
    }
    return null
  }

  updateSort (sort : TableSortType) : void {
    if (sort.sortBy === this.sort.sortBy) {
      this.sort.asc = !this.sort.asc
    } else {
      this.sort.asc = false
    }
    this.sort.sortBy = sort.sortBy
    this.$emit('sortChanged', this.sort)
  }

  onRowClick (row) {
    this.$emit('rowClicked', row)
  }

  onCellClick (cell) {
    this.$emit('cellClicked', cell)
  }
}
