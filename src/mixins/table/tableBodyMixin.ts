import { Vue, Component, Prop } from 'vue-property-decorator'
import { UnionBodyRowType } from '@/components/PTable/PTableTypes'

@Component({})
export default class PTableBodyMixin extends Vue {
  @Prop() bodyData : UnionBodyRowType[]

  getRowData (row : UnionBodyRowType) {
    return 'cells' in row ? row.cells : row
  }

  onRowClick (row) {
    this.$emit('rowClicked', row)
  }

  onCellClick (cell) {
    this.$emit('cellClicked', cell)
  }
}
