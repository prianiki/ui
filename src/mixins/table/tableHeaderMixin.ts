import { Vue, Component, Prop } from 'vue-property-decorator'
import { HeaderCellType, TableSortType } from '@/components/PTable/PTableTypes'

@Component({})
export default class PTableHeaderMixin extends Vue {
  @Prop() headerData: HeaderCellType[]
  @Prop() sort: TableSortType

  onClick (cell) {
    if (cell.sortBy) {
      this.$emit('updateSort', {
        sort: this.sort.sortBy,
        sortBy: cell.sortBy
      })
    }
  }
}
