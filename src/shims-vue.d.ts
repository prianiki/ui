declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module '*.svg?inline' {
  import { VueClass } from 'vue-class-component/lib/declarations'
  import { Vue } from 'vue/types/vue'
  const content: VueClass<Vue>
  export default content
}