export type RegisterInjectionType = {
  register : Function
  unregister : Function
} | null

export type TabType = {
  label : string
  value : string
}
