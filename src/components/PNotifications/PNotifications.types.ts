export type PNotification = {
  id: string
  message: string
  icon?: string
  delay?: number
  type?: string
  onDismiss?: Function
  dismissText?: string
  undo?: PNotificationUndo
  additionalClasses?: string
  notificationGroup?: string
}

export type PNotificationUndo = {
  text: string
  action?: Function
  component?: string
}
