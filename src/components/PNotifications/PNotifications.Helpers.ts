import { PNotification } from '@/components/PNotifications/PNotifications.types'
import { ID } from '@/utils/strings'
import Vue from 'vue'

export const pNotifications = new Vue({
  data: {
    notificationGroups: {
      default: [] as PNotification[]
    }
  },
  mounted () {
    // @ts-ignore
    this.$on('addNotification', this.addNotification)
    // @ts-ignore
    this.$on('removeNotification', this.removeNotification)
  },
  methods: {
    addNotificationGroup (key) {
      this.$set(this.notificationGroups, key, [])
    },
    addNotification (notification: PNotification, groupKey = 'default') {
      if (!notification.id) {
        notification.id = ID()
      }

      if (!notification.delay && notification.delay !== 0) {
        notification.delay = 10 * 1000
      }

      notification.notificationGroup = groupKey

      this.notificationGroups[groupKey].push(notification)
    },
    removeNotification (notification: PNotification | string) {
      let groupKey = 'default'
      if (typeof notification !== 'string' && notification.notificationGroup) {
        groupKey = notification.notificationGroup
      }

      let id: string

      if (typeof notification === 'string') {
        id = notification
      } else {
        id = notification.id
      }

      this.notificationGroups[groupKey] = this.notificationGroups[groupKey].filter(x => x.id !== id)
    }
  }
})

export const createPNotification = (message, type): PNotification => {
  return {
    id: ID(),
    message,
    type
  }
}
