import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import PNotificationsRoot from './PNotificationsRoot'
import PNotificationItem from './PNotificationItem'
import StoryBlockWrapper from './../StoryBlockWrapper'
import { createPNotification, pNotifications } from './PNotifications.Helpers'

storiesOf('PNotifications', module)
  .add('default', () => ({
    components: { PNotificationsRoot, StoryBlockWrapper, PNotificationItem },
    template: `
      <story-block-wrapper>
      <div>
      Group 1: 
        <p-notifications-root></p-notifications-root>
      </div>
      
      <div>
      Group 2: 
        <p-notifications-root notificationGroup="test">
            <template #icons>icons</template>
</p-notifications-root>
      </div>
      
       <button @click="addNotification">Add Notification</button>
      </story-block-wrapper>`,
    data () {
      return {
      }
    },
    mounted () {
      pNotifications.addNotificationGroup('test')
      const notification = createPNotification('hello world, i m forever', 'success')
      notification.delay = 0
      notification.icon = 'icon'
      notification.undo = {
        text: 'undo me',
        action: () => {
          alert('oh my luke')
        }
      }
      notification.dismissText = 'Close me'
      pNotifications.addNotification(notification)
    },
    methods: {
      addNotification () {
        const notification = createPNotification('hello world', 'success')
        notification.icon = 'hey'
        pNotifications.addNotification(notification, 'test')
      }
    }
  }))
