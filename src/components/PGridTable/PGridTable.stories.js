import { storiesOf } from '@storybook/vue'
import PInputBlock from '@/components/PInputBlock/PInputBlock.vue'
import PGridTable from '@/components/PGridTable/PGridTable.vue'
import StoryBlockWrapper from './../StoryBlockWrapper'
import sortArrow from '../../assets/img/sort.svg'
storiesOf('P Grid Table', module)
  .add('default', () => ({
    components: { PGridTable, StoryBlockWrapper, PInputBlock, sortArrow },
    template: `
      <story-block-wrapper>
        <p-grid-table
          :sizes="myStyle"
          :header="myHeader"
          :body="computedItems"
          :default-sort="defaultSort"
          @body-cell-on-click="onClick"

        >
          <template #header="{content, sort, id}">
            <div>
              <span v-html="content(sort, id)"></span>

            </div>
          </template>
          <template #firstName="{content}">
            <div>
              <span>{{ content.name }}</span>
              <span>{{ content.reverse }}</span>
            </div>
          </template>
        </p-grid-table>
      </story-block-wrapper>
    `,
    data () {
      return {
        myItems: [
          'Vladimir',
          'Ravil',
          'Andrew',
          'Genia'
        ],
        myHeader: {
          id: {
            content: this.contentFn
          },
          firstName: {
            content: this.contentFn
          }
        },
        defaultSort: {
          key: 'id.content',
          asc: true
        },
        myStyle: {
          headerHeight: '30px',
          bodyRowsHeight: '30px',
          columnsWidths: {
            id: '60px',
            firstName: '150px'
          }
        }
      }
    },
    computed: {
      computedItems () {
        return this.myItems.map((item, index) => {
          return {
            id: {
              content: index + 1,
              $class: 'id-class',
              $id: 'test2'
            },
            firstName: {
              content: {
                name: item,
                reverse: item.split('').reverse().join('')
              },
              $sort: 'reverse',
              $id: 'test'
            }
          }
        })
      }
    },
    methods: {
      contentFn: (sort, id) => {
        const sortKey = sort.key.split('.')[0]

        if (sortKey === '' || sortKey !== id) {
          return `${id}`
        } else if (sortKey === id && sort.asc) {
          return `${id} <img src=${sortArrow} >`
        } else {
          return `${id} <img src=${sortArrow} style="transform: rotate(180deg)">`
        }
      }
    },
    onClick ($id) {
      console.log($id)
    }
  }))
