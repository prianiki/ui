export const getTableStyle = ({ headerHeight, bodyRowsHeight }: Sizes, bodyLength) => {

  const gridTemplateRows =
    `${headerHeight || 'auto'} repeat(${bodyLength}, ${bodyRowsHeight || 'auto'})`

  return {
    display: 'grid',
    gridTemplateRows
  }
}

export const getRowStyle = (columnsWidths) => {

  const gridTemplateColumns = getColumnsFrom(columnsWidths)

  return {
    display: 'grid',
    gridTemplateColumns
  }
}

export const getColumnsFrom = (columnsWidths): string => {

  return Object.keys(columnsWidths).reduce((columns, column) => {

    return columns + ' ' + (columnsWidths[column] || 'auto')
  }, '')
}

export const validateColumnWidths = (bodyRow = {}, widths) => {
  return Object.keys(bodyRow).reduce((validated, key) => {

    const value = widths[key] || '1fr'
    return {
      ...validated,
      [key]: value
    }
  }, {})
}

export const cellClasses = (cell, context: string) => {

  const defaultClassName = slugifyClass(cell.name)

  return [
    `${context}-${defaultClassName}`,
    cell.$class
  ]
}

export const slugifyClass = (string) => string
  .replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2')
  .toLowerCase()

export const emptySizes = () => ({
  headerHeight: null,
  bodyRowsHeight: null,
  columnsWidths: {}
})


export type Sizes = {
  headerHeight?: string
  bodyRowsHeight?: string
  columnsWidths: {
    ['columnName']: string
  }
}


export type Sort = {
  key: string,
  asc: boolean
}
