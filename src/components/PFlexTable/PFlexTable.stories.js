import { storiesOf } from '@storybook/vue'

import PFlexTable from '@/components/PFlexTable/PFlexTable.vue'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PFlexTable', module)
  .add('default', () => ({
    components: { PFlexTable, StoryBlockWrapper },
    template: `<story-block-wrapper><p-flex-table /></story-block-wrapper>`
  }))
