import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import PRadio from './PRadio'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PRadio', module)
  .add('default', () => ({
    components: { PRadio, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
       <p-radio v-model="value" radioValue="hello">Check me = {{ value }}</p-radio>
       <p-radio v-model="value" radioValue="world">Check me = {{ value }}</p-radio>
      </story-block-wrapper>`,
    data () {
      return {
        value: false
      }
    }
  }))
