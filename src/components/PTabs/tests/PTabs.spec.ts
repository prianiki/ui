import { shallowMount } from '@vue/test-utils'
import PTabs from '../PTabs.vue'
import PTabsHeaderItem from '../PTabsHeaderItem.vue'
import PTabsItem from '../PTabsItem.vue'

describe('p tabs', () => {
  const wrapper = shallowMount(PTabs, {
    propsData: {
      tabs: [
        { label: 'label-1', value: 'test-1' },
        { label: 'label-2', value: 'test-2' },
        { label: 'label-3', value: 'test-3' }
      ],
      slots: {
        default: PTabsItem
      }
    }
  })

  it('should emit active tab while clicking and change local active tab', () => {
    wrapper.findAll(PTabsHeaderItem).at(1).trigger('click')

    // @ts-ignore
    expect(wrapper.emitted().input[0]).toEqual([wrapper.vm.tabs[1]])
    // @ts-ignore
    expect(wrapper.vm.localActiveTab).toEqual(wrapper.vm.tabs[1])
  })
})

describe('p tabs item', () => {
  const wrapper = shallowMount(PTabsItem, {
    provide: {
      _tabs () {
        return {
          activeTab: 'test'
        }
      }
    }
  })

  it('should take injected value', () => {
    // @ts-ignore
    expect(wrapper.vm.activeTab).toEqual('test')
  })
})
