import { storiesOf } from '@storybook/vue'

import PTabs from './PTabs'
import PTabsItem from './PTabsItem'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PTabs', module).add('default', () => ({
  components: { PTabs, PTabsItem, StoryBlockWrapper },
  template: `<story-block-wrapper>
    <p-tabs :tabs="tabs">
      <p-tabs-item tab="value-1">
        <div>tab 1</div>
      </p-tabs-item>
      <p-tabs-item tab="value-2">
        <div>tab 2</div>
      </p-tabs-item>
      <p-tabs-item tab="value-3">
        <div>tab 3</div>
      </p-tabs-item>
    </p-tabs>
  </story-block-wrapper>`,
  data () {
    return {
      tabs: [
        {
          label: 'label-1',
          value: 'value-1'
        },
        {
          label: 'label-2',
          value: 'value-2'
        },
        {
          label: 'label-3',
          value: 'value-3'
        }
      ]
    }
  }
}))
