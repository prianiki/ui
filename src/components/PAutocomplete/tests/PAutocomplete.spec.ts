import { mount } from '@vue/test-utils'
import PAutocomplete from '../PAutocomplete.vue'

const getElement = (props = {}) => {
  // @ts-ignore
  return mount(PAutocomplete, {
    propsData: {
      value: '',
      options: [
        { value: 'test', label: 'My name is Vladimir' },
        { value: 'test1', label: 'What is yours ?' }
      ],
      ...props
    }
  })
}
describe('pAutocomplete', () => {
  it('should mount', () => {
    const element = mount(PAutocomplete)

    expect(element.find('.p-autocomplete')).toBeTruthy()
    expect(element.find('input')).toBeTruthy()
  })

  it('should filter options', () => {
    const element = getElement()

    expect(element.props().options.length).toBe(2)

    element.setData({
      searchString: 'Vladimir'
    })

    // @ts-ignore
    expect(element.vm.filteredOptions.length).toBe(1)
    // @ts-ignore
    expect(element.vm.filteredOptions[0].value).toBe('test')
  })
})
