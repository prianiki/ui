import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import PAutocomplete from './PAutocomplete'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PAutocomplete', module)
  .add('default', () => ({
    components: { PAutocomplete, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
       <p-autocomplete :options="options" v-model="value" :multiple="false" :canAddOwnOption="true"></p-autocomplete>
       
       <p-autocomplete :options="options" v-model="value" @input="onInput" :multiple="true" :canAddOwnOption="true" :excludeSelected="true">
<!--        <template v-slot:dropdownItem="slotOptions">-->
<!--        <div @click="slotOptions.onClick(slotOptions.option)"> {{ slotOptions }} </div>-->
<!--</template>-->
        
        
</p-autocomplete>
       
       <p-autocomplete :options="options" v-model="value" :multiple="true" :canAddOwnOption="false"></p-autocomplete>
      </story-block-wrapper>`,
    data () {
      return {
        options: [],
        value: 'hello'
      }
    },
    mounted () {
      this.options.push({
        label: 'Hello',
        value: 'hello'
      })
      this.options.push({
        label: 'World',
        value: 'world'
      })
      this.options.push({
        label: 'Dude',
        value: 'girl'
      })
    },
    methods: {
      onInput (value) {
        console.log(value)
      }
    }
  }))
