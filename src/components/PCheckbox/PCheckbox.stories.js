import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import PCheckbox from './PCheckbox'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PCheckbox', module)
  .add('default', () => ({
    components: { PCheckbox, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
       <p-checkbox v-model="value">Check me = {{value}}</p-checkbox>
       <p-checkbox v-model="valueTrue">I'm checked = {{valueTrue}}</p-checkbox>
       <p-checkbox v-model="readonly" :readonly="true">I'm readonly (true) = {{readonly}}</p-checkbox>
      </story-block-wrapper>`,
    data () {
      return {
        value: false,
        valueTrue: true,
        readonly: true
      }
    }
  }))
