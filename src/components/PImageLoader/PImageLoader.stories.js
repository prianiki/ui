import { storiesOf } from '@storybook/vue'

import PImageLoader from './PImageLoader'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PImageLoader', module).add('default', () => ({
  components: { PImageLoader, StoryBlockWrapper },
  template: `<story-block-wrapper>
    <p-image-loader
      :imageURL="imageUrl"
      :on-new-file="onNewFile"
    />
  </story-block-wrapper>`,
  data () {
    return {
      imageUrl: ''
    }
  },
  methods: {
    onNewFile (file, rawFile) {
      this.imageUrl = rawFile
      console.log(file, rawFile)
    }
  }
}))
