import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import PSmartList from './PSmartList'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PSmartList', module)
  .add('default', () => ({
    components: { PSmartList, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
        <p-smart-list :items.sync="items">
            <template #default="{ item, index }">
                <div> item: {{ item }}
                    <input v-model="item.value">
                </div>
            </template>
        </p-smart-list>
      </story-block-wrapper>`,
    data () {
      return {
        items: [
          { _id: 'test', value: 'hello' }
        ]
      }
    }
  }))
