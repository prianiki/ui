import { SmartListItem } from '@/components/PSmartList/SmartList.types'

export const isObjectEmpty = (object: SmartListItem, exceptions: string[] | string | null = null) => {
  if (object) {
    let copy = { ...object }

    if (exceptions) {
      if (Array.isArray(exceptions)) {
        exceptions.forEach((x) => {
          copy[x] = null
        })
      } else {
        copy[exceptions] = null
      }
    }

    const hasNonEmpty = Object.values(copy).some((value) => {
      if (value) {
        if (typeof value === 'object') {
          if (Array.isArray(value)) {
            return !!value.length
          } else {
            if (value instanceof Date) {
              return true
            }
            return !isObjectEmpty(value, exceptions)
          }
        } else {
          return true
        }
      } else {
        return false
      }
    })

    return !hasNonEmpty
  }
}
