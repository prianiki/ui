import { mount } from '@vue/test-utils'
import PSmartList from '../PSmartList.vue'

describe('p-smart-list', () => {
  describe('basic tests', () => {
    it('should mount', async () => {
      const wrapper = mount(PSmartList, {})
      expect(wrapper.contains('.p-smart-list')).toBeTruthy()
    })

    it('should display provided items in the provided slot', async () => {
      const wrapper = getWrapper()
      expect(wrapper.contains('.p-smart-list')).toBeTruthy()
      expect(wrapper.contains('.item-in-slot')).toBeTruthy()
    })
  })

  // TODO: test adding new item
  // TODO: test deleting item
})

const getWrapper = (props?) => {
  return mount(
    PSmartList,
    {
      scopedSlots: {
        default: `
            <div class="item-in-slot" slot-scope="{ item, index }">{{ item }}</div>`
      },
      propsData: {
        items: [
          {
            _id: 'first',
            name: 'hello'
          },
          {
            _id: 'second',
            name: 'hello 2'
          }
        ],
        ...props
      }
    }
  )
}
