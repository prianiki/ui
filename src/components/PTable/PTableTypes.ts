export type HeaderCellType = {
  content : string
  colspan? : number
  class? : string
  sortBy? : string
  id? : string
  component? : string
}

export type BodyCellType = {
  content : string
  sortValue? : string
  sortKey? : string
  hide? : boolean
  component? : string
}

export type TableSortType = {
  sortBy : string | null
  asc : boolean
}

export type BodyRowType = {
  id? : string
  class? : string
  cells : BodyCellType[]
}

export type SimpleBodyRowType = BodyCellType[]

export type UnionBodyRowType = BodyRowType | SimpleBodyRowType
