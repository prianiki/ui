import { storiesOf } from '@storybook/vue'

import PTable from '@/components/PTable/PTable.vue'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PTable', module)
  .add('default', () => ({
    components: { PTable, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
        <p-table :body="tableBody">
          <template  #bodyCell="{ cell }">
            <component :is="cell.component" :cell="cell">
              {{ cell.content }}
            </component>
          </template>
        </p-table>
      </story-block-wrapper>`,
    data () {
      return {
        tableBody: [
          [
            {
              content: '4',
              sortValue: '3',
              sortKey: 'id'
            },
            {
              content: 'Vladimir',
              sortValue: 'vladimir',
              sortKey: 'name',
              component: 'h1'
            }
          ]
        ]
      }
    }
  }))
