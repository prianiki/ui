import { BodyRowType, HeaderCellType, SimpleBodyRowType } from '@/components/PTable/PTableTypes'

export const header : HeaderCellType[] = [
  {
    content: 'id',
    sortBy: 'id',
    class: 'header'
  },
  {
    content: 'name',
    sortBy: 'name',
    class: 'header'
  }
]

export const body : (BodyRowType | SimpleBodyRowType)[] = [
  {
    class: 'test-object',
    cells: [
      {
        content: '1',
        sortValue: '0',
        sortKey: 'id'
      },
      {
        content: 'Genya',
        sortValue: 'genya',
        sortKey: 'name'
      }
    ]
  },
  [
    {
      content: '2',
      sortValue: '1',
      sortKey: 'id'
    },
    {
      content: 'Olga',
      sortValue: 'olga',
      sortKey: 'name'
    }
  ], [
    {
      content: '3',
      sortValue: '2',
      sortKey: 'id'
    },
    {
      content: 'Katia',
      sortValue: 'katia',
      sortKey: 'name'
    }
  ],
  [
    {
      content: '4',
      sortValue: '3',
      sortKey: 'id'
    },
    {
      content: 'Vladimir',
      sortValue: 'vladimir',
      sortKey: 'name'
    }
  ],
  [
    {
      content: '5',
      sortValue: '4',
      sortKey: 'id'
    },
    {
      content: 'Andrey',
      sortValue: 'andrey',
      sortKey: 'name'
    }
  ]
]
