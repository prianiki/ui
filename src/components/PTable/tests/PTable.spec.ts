import { shallowMount } from '@vue/test-utils'
import PTable from '@/components/PTable/PTable.vue'
import PTableBody from '@/components/PTable/PTableBody/PTableBody.vue'
import PTableHeader from '@/components/PTable/PTableHeader/PTableHeader.vue'
import PTableBodyCell from '@/components/PTable/PTableBody/PTableBodyCell.vue'
import { Vue } from 'vue-property-decorator'

describe('p table body without slot', () => {
  const wrapper = shallowMount(PTableBody, {
    propsData: {
      bodyData: [{ content: 'test' }]
    }
  })
  const bodyData = [
    { cells: [{ content: 'test' }] },
    [{ content: 'test' }]
  ]

  it('should get row data right', () => {
    expect((wrapper.vm as any).getRowData(bodyData[0]))
      .toEqual((wrapper.vm as any).getRowData(bodyData[1]))
  })

  it('should show default if slot does not passed', () => {
    expect(wrapper.contains(PTableBodyCell)).toBeTruthy()
  })
})

describe('p table body with slot', () => {
  const wrapper = shallowMount(PTableBody, {
    propsData: {
      bodyData: [{ content: 'test' }]
    },
    slots: {
      default: '<div class="test"></div>'
    }
  })

  it('should show slots if it passed', () => {
    expect(wrapper.contains('.test')).toBeTruthy()
    expect(wrapper.contains(PTableBodyCell)).toBeFalsy()
  })
})

describe('p table', () => {
  const wrapper = shallowMount(PTable, {
    propsData: {
      body: [
        [{ sortKey: 'test', sortValue: 'test1' }],
        [{ sortKey: 'test', sortValue: 'test2' }]
      ],
      getMaxBy: 'test'
    }
  })

  const mockingSort = {
    sortBy: 'test',
    asc: false
  }

  it('should emit sort on change', () => {
    wrapper.find(PTableHeader).vm.$emit('updateSort', mockingSort)

    expect((wrapper.vm as any).sort).toEqual(mockingSort)
    expect(wrapper.emitted().sortChanged[0]).toEqual([mockingSort])
  })

  it('should sort table by getMaxBy prop', () => {
    expect(wrapper.emitted().maxValue[0]).toEqual(['test2'])
  })

  it('should restruct body data if sort changed', () => {
    expect((wrapper.vm as any).bodyData.reverse()).toEqual((wrapper.vm as any).body)
    wrapper.find(PTableHeader).vm.$emit('updateSort', {
      sortBy: 'test',
      asc: true
    })

    expect((wrapper.vm as any).bodyData).toEqual((wrapper.vm as any).body)
  })
})

describe('p table scoped slots', () => {
  const testVue = new Vue({
    template: `  
        <p-table :body="tableBody" :header="tableHeader">
          <template #headerCell="{ cell }">
            <component :is="cell.component" :cell="cell">
              {{ cell.content }}
            </component>
          </template>
          <template #headerSorter="{ cell }">
            <div class="sorter" :cell="cell">{{ cell.content }}</div>
          </template>
          <template #bodyCell="{ cell }">
            <component :is="cell.component" :cell="cell">
              {{ cell.content }}
            </component>
          </template>
        </p-table>`,
    components: { PTable },
    data () {
      return {
        tableBody: [
          [{ component: 'h3', content: 'test' }]
        ],
        tableHeader: [
          { component: 'h2', content: 'test2', sortBy: 'test' }
        ],
        sort: {
          sortBy: 'test',
          asc: false
        }
      }
    }
  }).$mount()

  it('should pass props into scoped slots', () => {
    // @ts-ignore
    expect(testVue.$el.querySelector('h3').innerHTML.trim())
      .toEqual('test')
    // @ts-ignore
    expect(testVue.$el.querySelector('h2').innerHTML.trim())
      .toEqual('test2')
    // @ts-ignore
    expect(testVue.$el.querySelector('.sorter').innerHTML.trim())
      .toEqual('test2')
  })
})
