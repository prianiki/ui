import { shallowMount } from '@vue/test-utils'
import PTableSorter from '@/components/PTable/PTableHeader/PTableSorter.vue'
import PTableHeader from '@/components/PTable/PTableHeader/PTableHeader.vue'
import PTableHeaderCell from '@/components/PTable/PTableHeader/PTableHeaderCell.vue'

describe('p table sorter without slot', () => {
  const wrapper = shallowMount(PTableSorter, {
    // required props
    propsData: { sort: { sortBy: null, asc: false }, sortBy: '' }
  })

  it('should show default slot if slot does not passed', () => {
    expect(wrapper.contains('.p-sorter__default-arrow')).toBeTruthy()
  })
})

describe('p table sorter with slot', () => {
  const wrapper = shallowMount(PTableSorter, {
    slots: {
      default: '<div class="test"></div>'
    },
    propsData: {
      sort: {
        sortBy: null,
        asc: false
      },
      sortBy: ''
    }
  })

  it('should be able to contain elements in slot', () => {
    expect(wrapper.contains('.test')).toBeTruthy()
    expect(wrapper.contains('.p-sorter__default-arrow')).toBeFalsy()
  })
})

describe('p table header without slot', () => {
  const wrapper = shallowMount(PTableHeader, {
    propsData: {
      headerData: [{ content: '' }]
    }
  })
  it('should show default if slot does not passed', () => {
    expect(wrapper.contains(PTableHeaderCell)).toBeTruthy()
  })
})

describe('p table header with slot', () => {
  const wrapper = shallowMount(PTableHeader, {
    propsData: {
      sort: {
        sortBy: 'test',
        asc: false
      },
      headerData: [
        {
          content: 'test',
          sortBy: 'test'
        }
      ]
    },
    slots: {
      default: '<div class="test"></div>'
    }
  })

  it('should emit update sort while click', () => {
    wrapper.find('.p-th').trigger('click')

    expect(wrapper.emitted().updateSort[0]).toEqual([{
      sort: 'test',
      sortBy: 'test'
    }])
  })

  it('should be able to contains elements in slots', () => {
    expect(wrapper.contains('.test')).toBeTruthy()
    expect(wrapper.contains('p-table-header-cell')).toBeFalsy()
  })
})
