import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import PDropdown from './PDropdown'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PDropdown', module)
  .add('default', () => ({
    components: { PDropdown, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
       <p-dropdown :options="options" v-model="value" :multiple="true"></p-dropdown>
      <p-dropdown :options="options" v-model="value" :multiple="true" :readonly="true"></p-dropdown>
      <p-dropdown :options="options" v-model="value" :multiple="true" :allow-empty="false"></p-dropdown>
      </story-block-wrapper>`,
    data () {
      return {
        options: [
          {
            label: 'Hello',
            value: 'hello'
          },
          {
            label: 'World',
            value: 'world'
          }
        ],
        value: ''
      }
    }
  }))
