import { shallowMount } from '@vue/test-utils'
import PDropdown from '../PDropdown.vue'
const getWrapper = (props = {}) => {
  return shallowMount(PDropdown, {
    propsData: {
      value: '',
      options: [
        { label: 'test', value: 'test' },
        { label: 'test1', value: 'test1' }
      ],
      ...props
    }
  })
}
describe('PDropdown', () => {
  it('should mount', () => {
    const wrapper = getWrapper()
    expect(wrapper.find('.p-dropdown .p-dropdown__input').text()).toBeTruthy()
  })

  it('should display the list of options', () => {
    const wrapper = getWrapper()

    // @ts-ignore
    expect(wrapper.vm.doShowDropdown).toBe(false)

    const input = wrapper.find('.p-dropdown__input')
    input.trigger('click')
    // @ts-ignore
    expect(wrapper.vm.doShowDropdown).toBe(true)

    expect(wrapper.find('.p-dropdown__item__container')).toBeTruthy()
    expect(wrapper.findAll('.p-dropdown__item').length).toBe(2)
  })

  it('should select an option', () => {
    const wrapper = getWrapper()
    const input = wrapper.find('.p-dropdown__input')
    input.trigger('click')

    const firstItem = wrapper.find('.p-dropdown__item')
    firstItem.trigger('click')

    expect(wrapper.emitted().input).toBeTruthy()
    expect(wrapper.emitted().input[0]).toEqual(['test'])
  })
})
