export type DropdownOption = {
  label: string
  value: InputValue
  isSelected?: boolean
}

export type DropdownValue = InputValue | InputValue[]

export type InputValue = string | number | boolean
