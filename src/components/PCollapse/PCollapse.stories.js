import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import PCollapse from './PCollapse'
import PTable from '../PTable/PTable'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PCollapse', module)
  .add('default', () => ({
    components: { PCollapse, PTable, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
        <div @click="showTable = !showTable">show/hide</div>
        <p-collapse :value="showTable">
          <p-table />
        </p-collapse>
      </story-block-wrapper>`,
    data () {
      return {
        showTable: false
      }
    }
  }))
  .add('with element to collapse', () => ({
    components: { PCollapse, PTable, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
        <div class="test-div" style="width: 100%; display: flex">
          <div class="test">another div</div>
          <div @click="showTable = !showTable" class="button">show/hide</div>
          <p-collapse
            :value="showTable"
            :duration="1000"
            direction="width"
            elementToCollapse=".test-div"
            :excludeSizeOf="['.button', '.test']"
          >
            <div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae orci ipsum. Aliquam at sapien ipsum. Nunc semper eget orci vitae egestas. Morbi congue porttitor purus et euismod. Sed maximus odio dignissim dui aliquam dapibus. Nullam bibendum nunc imperdiet efficitur suscipit. Aenean suscipit lorem non mi accumsan fringilla. Pellentesque auctor eros ac elementum euismod. Nunc luctus augue nulla, non elementum massa ultricies at. Donec in turpis ultrices, consequat nunc eget, scelerisque metus.
                Aliquam erat volutpat. Pellentesque fringilla imperdiet blandit. Aenean tempus ornare odio. Fusce euismod, nisl in vehicula rutrum, leo turpis scelerisque mauris, sed vulputate arcu arcu at enim. Maecenas egestas maximus mollis. Vestibulum porta ex ac turpis pharetra elementum. Vestibulum malesuada vestibulum mi sit amet porta.</p>
            </div>
          </p-collapse>
        </div>
      </story-block-wrapper>`,
    data () {
      return {
        showTable: false
      }
    }
  }))
