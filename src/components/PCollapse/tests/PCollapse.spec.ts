import { shallowMount } from '@vue/test-utils'
import PCollapse from '../PCollapse.vue'

describe('p collapse', () => {
  const wrapper = shallowMount(PCollapse, {
    slots: {
      default: '<div class="test"></div>'
    }
  })

  it('should open if value true and vice versa', () => {
    expect(wrapper.contains('.test')).toBeFalsy()
    wrapper.setProps({ value: true })
    expect(wrapper.contains('.test')).toBeTruthy()
  })
})
