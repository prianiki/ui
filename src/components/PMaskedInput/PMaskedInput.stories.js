import { storiesOf } from '@storybook/vue'

import PMaskedInput from './PMaskedInput'
import StoryBlockWrapper from './../StoryBlockWrapper'
import { dateFilter } from '../../utils/filters/dateFilter'

storiesOf('PMaskedInput', module).add('default', () => ({
  components: { PMaskedInput, StoryBlockWrapper },
  template: `<story-block-wrapper>
    <p-masked-input
      v-model="value"
      :filter="dateFilter"
    />
  </story-block-wrapper>`,
  data () {
    return {
      value: '',
      dateFilter: dateFilter
    }
  }
}))
