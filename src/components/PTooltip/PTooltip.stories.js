import { storiesOf } from '@storybook/vue'

import PTooltipTestArea from './PTooltipTestArea'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PTooltip', module)
  .add('default', () => ({
    components: { PTooltipTestArea, StoryBlockWrapper },
    template: `
      <story-block-wrapper>
        <p-tooltip-test-area></p-tooltip-test-area>
      </story-block-wrapper>`,
    data () {
      return {
        options: [],
        value: 'hello'
      }
    },
    mounted () {
      this.options.push({
        label: 'Hello',
        value: 'hello'
      })
      this.options.push({
        label: 'World',
        value: 'world'
      })
      this.options.push({
        label: 'Dude',
        value: 'girl'
      })
    },
    methods: {
      onInput (value) {
        console.log(value)
      }
    }
  }))
