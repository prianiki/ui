import { mount } from '@vue/test-utils'
import PExpander from '../PExpander.vue'
import {
  getBoundaries,
  getDefaultBoundaries,
  getDefaultPosition, getPositionFromStyle,
  getXAvailableDirections,
  getXPosition,
  getYAvailableDirections,
  getYPosition,
  parseDefaultPositionString,
  verifyPositions,
  xPositions, xPositionsCenter,
  xPositionsEnum,
  yPositions,
  yPositionsEnum
} from '@/components/PTooltip/ptooltip.helpers'

describe('PExpander', () => {

  it('should mount', () => {
    const expander = mount(PExpander)

    expect(expander.find('.p-expander')).toBeTruthy()

    const styles = (expander.vm as any).styles
    expect(styles).toBeDefined()
  })

  it('verifyPositions should check if the positions are possible, if not switch them', () => {
    const validPositions = verifyPositions({
      x: xPositionsEnum.LEFT,
      y: yPositionsEnum.TOP
    })

    expect(validPositions).toEqual({
      x: xPositionsEnum.LEFT,
      y: yPositionsEnum.TOP
    })

    const invertedPositions = verifyPositions({
      // @ts-ignore
      x: yPositionsEnum.TOP,
      // @ts-ignore
      y: xPositionsEnum.LEFT
    })

    expect(invertedPositions).toEqual({
      x: xPositionsEnum.LEFT,
      y: yPositionsEnum.TOP
    })

    const invalidXPositions = verifyPositions({
      // @ts-ignore
      x: '50',
      y: yPositionsEnum.TOP
    })
    expect(invalidXPositions).toEqual({
      x: xPositionsEnum.CENTER,
      y: yPositionsEnum.TOP
    })

    const invalidYPositions = verifyPositions({
      // @ts-ignore
      x: xPositionsEnum.RIGHT,
      // @ts-ignore
      y: '50'
    })
    expect(invalidYPositions).toEqual({
      x: xPositionsEnum.RIGHT,
      y: yPositionsEnum.TOP
    })

    const invalidPositions = verifyPositions({
      // @ts-ignore
      x: '50',
      // @ts-ignore
      y: '50'
    })
    expect(invalidPositions).toEqual(getDefaultPosition())

    const defaultPosition = getDefaultPosition()
    expect(defaultPosition).toEqual({
      x: xPositionsEnum.CENTER,
      y: yPositionsEnum.TOP
    })
  })

  it('parseDefaultPositionString should transform a string into an object', () => {
    const positionTopLeft = parseDefaultPositionString('top left')
    expect(positionTopLeft.x).toEqual(xPositionsEnum.LEFT)
    expect(positionTopLeft.y).toEqual(yPositionsEnum.TOP)

    const positionInverted = parseDefaultPositionString('left top')
    expect(positionInverted.x).toEqual(xPositionsEnum.LEFT)
    expect(positionInverted.y).toEqual(yPositionsEnum.TOP)

    const positionInvalid = parseDefaultPositionString('hello')
    expect(positionInvalid.x).toEqual(xPositionsEnum.CENTER)
    expect(positionInvalid.y).toEqual(yPositionsEnum.TOP)
  })

  it('getYAvailableDirections should give an array of possible directions to expand to', () => {
    const availableDirections = getYAvailableDirections(getDefaultBoundaries())

    expect(availableDirections.length).toBe(1)
    expect(availableDirections[0]).toBe(yPositionsEnum.CENTER)
  })

  it('getYPosition should give the yPosition', () => {
    const yPosition = getYPosition(getDefaultBoundaries(), getDefaultPosition())
    expect(yPosition).toEqual({
      top: 'auto',
      bottom: '100%'
    })

    const yPositionBottom = getYPosition(getDefaultBoundaries(), {
      x: xPositionsEnum.RIGHT,
      y: yPositionsEnum.BOTTOM
    })
    expect(yPositionBottom).toEqual(yPositions.bottom)

    const yPositionCenter = getYPosition(getDefaultBoundaries(), {
      x: xPositionsEnum.RIGHT,
      y: yPositionsEnum.CENTER
    })
    expect(yPositionCenter).toEqual(yPositions.center)

    const yPositionTopFallbackToBottom = getYPosition(
      {
        ...getDefaultBoundaries(),
        bottom: true
      }, getDefaultPosition())
    expect(yPositionTopFallbackToBottom).toEqual(yPositions.bottom)
  })

  it('getXAvailableDirections should give an array of possible directions to expand to', () => {
    const availableDirections = getXAvailableDirections({
      ...getDefaultBoundaries(),
      width: 100
    }, yPositionsEnum.TOP)

    expect(availableDirections.length).toBe(0)

    const availableDirectionsRight = getXAvailableDirections({
      ...getDefaultBoundaries(),
      right: 10
    }, yPositionsEnum.TOP)
    expect(availableDirectionsRight.includes(xPositionsEnum.RIGHT)).toBeTruthy()
  })

  it('getXPosition should give the yPosition', () => {
    const xPosition = getXPosition(getDefaultBoundaries(), getDefaultPosition(), yPositionsEnum.TOP)
    expect(xPosition).toEqual(xPositions.center)

    const xPositionRight = getXPosition({
      ...getDefaultBoundaries(),
      right: 10
    }, {
      x: xPositionsEnum.RIGHT,
      y: yPositionsEnum.TOP
    }, yPositionsEnum.CENTER)

    expect(xPositionRight).toEqual(xPositionsCenter.right)
  })

  it('getPositionFromStyle should return a simple position given a set of styles', () => {
    // @ts-ignore
    const position = getPositionFromStyle({
      ...xPositions.right,
      ...yPositions.top
    })

    expect(position).toEqual({
      x: xPositionsEnum.RIGHT,
      y: yPositionsEnum.TOP
    })
  })

  it('getBoundaries should return the boundaries', () => {
    const elementBoundingClientRect = {
      height: 10,
      width: 100,
    } as ClientRect

    const parentBoundingClientRect = {
      left: 300,
      right: 400,
      height: 20,
      width: 100,
      top: 300,
      bottom: 320
    } as ClientRect

    const expandToBoundingClientRect = {
      left: 0,
      right: 600,
      height: 600,
      width: 600,
      top: 0,
      bottom: 600
    } as ClientRect

    const elementOffsetHeight = 10

    const boundaries = getBoundaries({
      elementBoundingClientRect,
      parentBoundingClientRect,
      elementOffsetHeight,
      expandToBoundingClientRect
    })
    expect(boundaries.top).toBe(true)
    expect(boundaries.right).toBe(expandToBoundingClientRect.right - parentBoundingClientRect.right)
    expect(boundaries.left).toBe(parentBoundingClientRect.left - expandToBoundingClientRect.left)
  })
})
