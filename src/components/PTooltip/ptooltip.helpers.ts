import { isInEnum } from '@/utils/index'

export type Boundaries = {
  top: boolean
  right: number
  bottom: boolean
  left: number
  width: number
  parentWidth: number
}

export enum xPositionsEnum {
  LEFT = 'left',
  CENTER = 'center',
  RIGHT = 'right'
}

export enum yPositionsEnum {
  TOP = 'top',
  CENTER = 'center',
  BOTTOM = 'bottom'
}

export type SimplePosition = {
  y: yPositionsEnum
  x: xPositionsEnum
}

export type Offset = {
  xOffset: number
  yOffset: number
}

export type OffsetStyle = {
  marginTop: number | string
  marginRight: number | string
  marginBottom: number | string
  marginLeft: number | string
}

export const getOffsetStyle = (position: SimplePosition, { xOffset, yOffset }: Offset): OffsetStyle => {
  let offset: OffsetStyle = {
    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0
  }

  if (position.x === 'left') {
    offset.marginRight = xOffset + 'px'
  } else {
    offset.marginLeft = xOffset + 'px'
  }

  if (position.y === 'top') {
    // @ts-ignore
    offset.marginBottom = yOffset + 'px'
  } else {
    if (position.y !== 'center') {
      // @ts-ignore
      offset.marginTop = yOffset + 'px'
    } else {
      // @ts-ignore
      offset.marginTop = -yOffset + 'px'
    }
  }

  return offset
}

export const getDefaultPosition = (): SimplePosition => {
  return {
    x: xPositionsEnum.CENTER,
    y: yPositionsEnum.TOP
  }
}

export const verifyPositions = (position: SimplePosition): SimplePosition => {
  if (isInEnum(position.y, yPositionsEnum) && isInEnum(position.x, xPositionsEnum)) {
    return position
  } else {
    if (isInEnum(position.x, yPositionsEnum) && isInEnum(position.y, xPositionsEnum)) {
      return {
        // @ts-ignore
        x: position.y,
        // @ts-ignore
        y: position.x
      }
    } else if (isInEnum(position.y, yPositionsEnum)) {
      return {
        y: position.y,
        x: getDefaultPosition().x
      }
    } else if (isInEnum(position.x, xPositionsEnum)) {
      return {
        x: position.x,
        y: getDefaultPosition().y
      }
    } else {
      return getDefaultPosition()
    }
  }

}

export const parseDefaultPositionString = (defaultPosition: string): SimplePosition => {
  const regex = /(\w+)/gi
  const positionArray = String(defaultPosition).match(regex)

  if (!positionArray) {
    return getDefaultPosition()
  }

  return verifyPositions({
    x: positionArray[1] as xPositionsEnum,
    y: positionArray[0] as yPositionsEnum
  })
}


export const getDefaultBoundaries = (): Boundaries => {
  return {
    top: false,
    right: 0,
    bottom: false,
    left: 0,
    width: 0,
    parentWidth: 0
  }
}
export type yPositionStyle = {
  top: '50%' | 'auto' | '100%'
  bottom: 'auto' | '100%'
}
export type xPositionStyle = {
  left: '100%' | 'auto' | '50%' | 0
  right: '100%' | 'auto' | '50%' | 0
}

export const getYPosition = (boundaries: Boundaries, defaultPosition: SimplePosition): yPositionStyle => {
  let expandTo = defaultPosition.y
  const availableDirections = getYAvailableDirections(boundaries)

  if (availableDirections.includes(expandTo) || availableDirections.length === 1) {
    // @ts-ignore
    return yPositions[expandTo]
  } else {
    // @ts-ignore
    return yPositions[availableDirections[1]]
  }
}

export const yPositions = {
  top: {
    top: 'auto',
    bottom: '100%'
  },
  bottom: {
    top: '100%',
    bottom: 'auto'
  },
  center: {
    top: '50%',
    bottom: 'auto'
  }
}

export const getYAvailableDirections = (boundaries: Boundaries): yPositionsEnum[] => {
  let availableDirections: yPositionsEnum[] = [
    yPositionsEnum.CENTER
  ]

  if (boundaries.top) {
    availableDirections.push(yPositionsEnum.TOP)
  }
  if (boundaries.bottom) {
    availableDirections.push(yPositionsEnum.BOTTOM)
  }

  return availableDirections
}

export const getXAvailableDirections = (boundaries: Boundaries, yPosition: yPositionsEnum): xPositionsEnum[] => {
  let availableDirections: xPositionsEnum[] = []

  if ((yPosition === yPositionsEnum.TOP || yPosition === yPositionsEnum.BOTTOM) && canFitInCenter(boundaries)) {
    availableDirections.push(xPositionsEnum.CENTER)
  }

  if (boundaries.right && boundaries.right > boundaries.width) {
    availableDirections.push(xPositionsEnum.RIGHT)
  }

  if (boundaries.left && boundaries.left > boundaries.width) {
    availableDirections.push(xPositionsEnum.LEFT)
  }

  return availableDirections
}

const canFitInCenter = (boundaries: Boundaries): boolean => {
  const halfWidth = boundaries.width / 2
  return halfWidth <= boundaries.right && halfWidth <= boundaries.left
}

export const getXPosition = (boundaries: Boundaries, defaultPosition: SimplePosition, yPosition: yPositionsEnum): xPositionStyle => {
  let expandTo = defaultPosition.x
  const availableDirections = getXAvailableDirections(boundaries, yPosition)

  const positions = yPosition === yPositionsEnum.CENTER ? xPositionsCenter : xPositions

  if (availableDirections.includes(expandTo) || availableDirections.length === 0) {
    // @ts-ignore
    return positions[expandTo]
  } else {
    // @ts-ignore
    return positions[availableDirections[0]]
  }
}

export const xPositionsCenter = {
  left: {
    left: 'auto',
    right: '100%'
  },
  right: {
    left: '100%',
    right: 'auto'
  },
  center: {
    left: '50%',
    right: 'auto'
  }
}

export const xPositions = {
  left: {
    left: 'auto',
    right: 0
  },
  right: {
    left: 0,
    right: 'auto'
  },
  center: {
    left: '50%',
    right: 'auto'
  }
}

export const getPositionFromStyle = (styles: xPositionStyle & yPositionStyle): SimplePosition => {
  const yPosition = getYPositionFromStyle(styles)

  const localXPositions = yPosition === yPositionsEnum.CENTER ? xPositionsCenter : xPositions
  let xPosition
  if (styles.left === localXPositions.left.left) {
    xPosition = xPositionsEnum.LEFT
  } else if (styles.left === localXPositions.right.left) {
    xPosition = xPositionsEnum.RIGHT
  } else {
    xPosition = xPositionsEnum.CENTER
  }

  return {
    x: xPosition,
    y: yPosition
  }
}

export const getYPositionFromStyle = (styles: yPositionStyle): yPositionsEnum => {
  let yPosition
  if (styles.top === yPositions.top.top) {
    yPosition = yPositionsEnum.TOP
  } else if (styles.top === yPositions.bottom.top) {
    yPosition = yPositionsEnum.BOTTOM
  } else {
    yPosition = yPositionsEnum.CENTER
  }

  return yPosition
}

export const getBoundaries = ({
  elementBoundingClientRect,
  parentBoundingClientRect,
  elementOffsetHeight,
  expandToBoundingClientRect,
  yOffset = 0,
  xOffset = 0
}: GetBoundariesArgs): Boundaries => {
  return {
    top: (parentBoundingClientRect.top - elementOffsetHeight - yOffset) > expandToBoundingClientRect.top,
    right: expandToBoundingClientRect.right - parentBoundingClientRect.right - xOffset,
    bottom: (parentBoundingClientRect.bottom + elementBoundingClientRect.height + yOffset) < expandToBoundingClientRect.bottom,
    left: parentBoundingClientRect.left - expandToBoundingClientRect.left - xOffset,
    width: elementBoundingClientRect.width,
    parentWidth: parentBoundingClientRect.width
  }
}

type GetBoundariesArgs = {
  elementBoundingClientRect: ClientRect | DOMRect
  parentBoundingClientRect: ClientRect | DOMRect
  elementOffsetHeight: number
  expandToBoundingClientRect: ClientRect | DOMRect
  yOffset?: number
  xOffset?: number
}

export const getCenterTransform = (positionStyle: yPositionStyle & xPositionStyle): string => {
  if (positionStyle.left === '50%' && positionStyle.top === '50%') {
    return 'translate(-50%, -50%)'
  } else if (positionStyle.left === '50%') {
    return 'translateX(-50%)'
  } else if (positionStyle.top === '50%') {
    return 'translateY(-50%)'
  }

  return ''
}
