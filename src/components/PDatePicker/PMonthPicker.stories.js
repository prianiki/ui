import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import PMonthPicker from './PMonthPicker'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PMonthPicker', module)
  .add('default', () => ({
    components: { PMonthPicker, StoryBlockWrapper },
    template: `<story-block-wrapper><p-month-picker v-model="value">
        <span slot="dropdownArrowL"><</span>
        <span slot="dropdownArrowR">></span>
    </p-month-picker></story-block-wrapper>
`,
    data () {
      return {
        value: new Date()
      }
    }
  }))
