import { storiesOf } from '@storybook/vue'

import PDatePicker from './PDatePicker'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PDatePicker', module)
  .add('default', () => ({
    components: { PDatePicker, StoryBlockWrapper },
    template: `<story-block-wrapper>
        <p-date-picker v-model="value"></p-date-picker>
    </story-block-wrapper>
`,
    data () {
      return {
        value: new Date()
      }
    }
  }))
