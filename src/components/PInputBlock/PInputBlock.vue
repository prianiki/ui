<template lang="pug">
  .p-input-block(
    :class="classes"
    @click="focusInput"
  )
    .p-input-block__label(v-if="label")
      .p-input-block__label__text(v-html="label")

    .p-input-block__input-group
      .p-input-block__input-group__wrapper
        .p-input-block__input-group__container
          .p-input-block__input-group__suggestion(
            v-if="suggestion"
            v-html="suggestionFormatted"
          )
          component.p-input-block__input-group__input(
            v-bind="{ ...$attrs }"
            :is="tag"
            :name="name"
            ref="input"
            :value="inputValue"
            :placeholder="placeholder"
            :required="required"
            :type="type"
            :disabled="disabled"
            :readonly="readonly"
            @input="inputValue = $event.target.value"
            @focus="onFocus"
            @blur="onBlur"
            @keyup.46="onDelete"
            @keyup.8="onBackspace"
            @keyup="onKeyup"
            v-autosize="autosize"
          )

          .p-input-block__input-group__right-button(
            v-if="$slots.rightButton"
            @click="onRightButtonClick"
          )
            slot(name="rightButton")

    transition(:name="errorTransition")
      .p-input-block__error(v-if="showError")
        slot(name="error" :errorMessage="errorMessage")
          .p-input-block__error__default(v-html="errorMessage")
</template>

<script lang="ts">
import { Vue, Component, Prop, Watch, Inject } from 'vue-property-decorator'
import { ForceErrorType, ValidatorFunction, ValueType } from './PInputBlockTypes'
import { isStringEmpty } from '@/utils/strings'
import autosize from '@/directives/autosize/autosize'
import { RegisterInjectionType } from '@/globalTypes'
import { getCaretPosition, keepCaretPosition } from '@/utils/caretPosition'

@Component({
  inheritAttrs: false,
  directives: {
    autosize: autosize
  }
})
export default class PInputBlock extends Vue {
  _uid : number

  $refs: {
    input: HTMLInputElement
  }

  @Prop() label : string | boolean
  @Prop() value : ValueType
  @Prop({ default: 'text' }) type : string
  @Prop() placeholder : string
  @Prop() name : string
  @Prop({ default: false }) readonly : boolean
  @Prop({ default: false }) inputHasBorder : boolean
  @Prop({ default: false }) hideBorder : boolean
  @Prop({ default: false }) autosize : boolean
  @Prop({ default: false }) disabled : boolean
  @Prop({ default: false }) required : boolean
  @Prop({ default: false }) forceRequired : boolean
  @Prop({ default: 'cannot be empty' }) requiredText : string
  @Prop({ default: 'invalid input' }) errorText : string
  @Prop({
    default () {
      return {
        active: false,
        text: 'invalidInput'
      }
    }
  }) forceError : ForceErrorType
  @Prop() suggestion : string
  @Prop({ default: 'input' }) tag : string
  @Prop({
    default () {
      return (value) => value
    }
  }) mask : Function
  @Prop({
    default () {
      return () => true
    }
  }) validator : ValidatorFunction
  @Prop({
    default: 'p-input-block__error-transition'
  }) errorTransition: string
  @Prop({
    default: null
  }) forceFocus: boolean | null
  @Prop({ default: false }) selectOnFocus: boolean

  hasFocus : boolean = false
  // serves to prevent showing errors before the element had focus
  hadFocus : boolean = false
  isValid : boolean = false
  defaultValue : ValueType = null

  @Inject({ default: null }) form : RegisterInjectionType

  created () {
    this.form && this.form.register(this)
  }

  beforeDestroy () {
    this.form && this.form.unregister(this)
  }

  mounted () {
    if (this.validateInput) {
      this.validateInput()
    }

    this.$emit('onIsValidChange', this.isValid)

    if (this.$refs.input) {
      this.$refs.input.value = this.inputValue
    }
  }

  @Watch('value')
  valueHandler (val: ValueType) {
    this.inputValue = val
  }

  @Watch('isValid', { immediate: true })
  isValidHandler (isValid: boolean) {
    this.$emit('onIsValidChange', isValid)
  }

  @Watch('hasFocus', { immediate: true })
  async onFocusChange (hasFocus) {
    await this.$nextTick()

    this.emitFocus(hasFocus)

    const hasFocusChanged = this.assertForcedFocus(this.forceFocus, hasFocus)

    return hasFocusChanged
      ? null
      : this.updateFocus(hasFocus)
  }

  @Watch('forceFocus', { immediate: true })
  onForceFocusChange (forceFocus) {
    if (forceFocus !== null) {
      if (this.$refs.input) {
        if (forceFocus) {
          this.$refs.input.focus()
          this.hasFocus = true
        } else {
          this.$refs.input.blur()
          this.hasFocus = false
        }
      }
    }
  }

  get inputValue () {
    return this.mask(this.value)
  }
  set inputValue (val) {
    const previousValue = this.inputValue
    const caretPosition = this.$refs.input && getCaretPosition(this.$refs.input)

    const maskedValue = this.mask(val)

    this.$emit('input', maskedValue)

    if (this.validateInput) {
      this.validateInput(maskedValue)
    }

    if (this.$refs.input) {
      this.$refs.input.value = maskedValue

      if (maskedValue !== val && this.hasFocus) {
        this.$nextTick(() => {
          keepCaretPosition(this.$refs.input, caretPosition, maskedValue, String(val), previousValue)
        })
      }
    }
  }

  get classes () {
    return {
      'p-input-block--has-focus': this.hasFocus,
      'p-input-block--has-error': this.showError,
      'p-input-block--input-border': this.inputHasBorder,
      'p-input-block--hide-border': this.hideBorder,
      'p-input-block--autosize': this.autosize,
      'p-input-block--readonly': this.readonly
    }
  }

  get showError () {
    // if we had the focus  and input is not valid
    if (this.hadFocus && !this.isValid) {
      return true
    }
    // if we have a forced error
    if (this.forceError.active || this.forceRequired) {
      return true
    }
    return false
  }

  get errorMessage () {
    if (this.forceError.active) {
      return this.forceError.text
    } else if (this.required && isStringEmpty(this.value)) {
      return this.requiredText
    } else {
      return this.errorText
    }
  }

  get suggestionFormatted () {
    const placeholder = this.suggestion.split('')
    const input = this.value
      ? String(this.value)
      : ''

    let output = ''

    for (let i = 0; i < placeholder.length; i++) {
      const letter = placeholder[i]
      if (i < input.length) {
        output += `<span class="masked-letter">${input[i]}</span>`
      } else {
        output += `<span class="future-letter">${letter}</span>`
      }
    }

    if (input.length === this.suggestion.length) {
      return ''
    } else {
      return output
    }
  }

  reset () {
    this.$nextTick(() => {
      this.hasFocus = false
      this.hadFocus = false
      this.isValid = true
      this.inputValue = this.defaultValue
    })
  }

  focusInput () {
    this.$refs.input.focus()
  }

  blurInput () {
    this.$refs.input.blur()
  }

  onFocus () {
    if (this.readonly) {
      return
    }
    this.hasFocus = true
  }

  onBlur () {
    if (this.readonly || this.forceFocus === true) {
      return
    }
    this.$nextTick(() => {
      this.hasFocus = false
      this.hadFocus = true
      if (this.validateInput) {
        this.validateInput()
      }
    })
  }

  validateInput (value? : string | number) {
    if (value == null) {
      // @ts-ignore
      value = this.inputValue
    }

    if (this.required) {
      this.isValid = !isStringEmpty(value)
    } else {
      this.isValid = this.validator(value)
    }
  }

  onRightButtonClick () {
    this.$emit('rightButtonClick')
  }

  onDelete (e) {
    this.$emit('delete', e)
  }

  onBackspace (e) {
    this.$emit('backspace', e)
  }

  onKeyup (e) {
    this.$emit('keyup', e)
  }

  assertForcedFocus (forceFocus: boolean | null, hasFocus: boolean): boolean {
    if (forceFocus == null) {
      return false
    }

    const isDifferent = forceFocus !== hasFocus

    isDifferent
      ? this.hasFocus = forceFocus
      : null

    return isDifferent
  }

  updateFocus (hasFocus) {
    if (this.$refs.input) {
      if (hasFocus) {
        this.focusInput()
        if (this.selectOnFocus) {
          this.$refs.input.select()
        }
      } else {
        this.blurInput()
      }
    }
  }

  emitFocus (hasFocus) {
    this.$emit('updateFocus', hasFocus)

    return hasFocus
      ? this.$emit('focus', hasFocus)
      : this.$emit('blur', hasFocus)
  }
}
</script>

<style lang="sass" scoped>
.p-input-block__input-group
  position: relative
  &__right-button
    position: absolute
    top: 50%
    right: 0
    transform: translateY(-50%)
</style>
