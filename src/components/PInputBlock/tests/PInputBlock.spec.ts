import { shallowMount } from '@vue/test-utils'
import PInputBlock from '../PInputBlock.vue'

describe('p input block', () => {
  const wrapper = shallowMount(PInputBlock, {
    propsData: {
      label: 'test'
    },
    slots: {
      rightButton: '<div>test</div>'
    },
    provide: {
      form: {
        register () {
          return 'register'
        },
        unregister () {
          return 'unregister'
        }
      }
    }
  })

  const input = wrapper.find('input')

  it('should display label if it passed in props', () => {
    expect(wrapper.contains('.p-input-block__label')).toBeTruthy()
  })

  it('should emit input value on input', () => {
    input.setValue('test')
    expect(wrapper.emitted().input[0][0]).toEqual('test')
  })

  it('should emit update focus while input on focus and on blur', async () => {
    input.trigger('focus')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().updateFocus[1][0]).toBe(true)

    input.trigger('blur')
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().updateFocus[2][0]).toBe(false)
  })

  it('should emit right button click', () => {
    wrapper.find('.p-input-block__input-group__right-button').trigger('click')

    expect(wrapper.emitted().rightButtonClick[0][0]).toEqual(undefined)
  })

  it('should be able to inject form value', () => {
    // @ts-ignore
    if (wrapper.vm.form) {
      // @ts-ignore
      expect(wrapper.vm.form.register()).toEqual('register')
      // @ts-ignore
      expect(wrapper.vm.form.unregister()).toEqual('unregister')
    }
  })
})
