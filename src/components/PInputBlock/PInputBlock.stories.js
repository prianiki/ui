import { storiesOf } from '@storybook/vue'

import PInputBlock from './PInputBlock'
import StoryBlockWrapper from './../StoryBlockWrapper'
import { formatNumeral } from '../../utils/numerals'

storiesOf('PInputBlock', module).add('default', () => ({
  components: { PInputBlock, StoryBlockWrapper },
  template: `<story-block-wrapper>
<!--    <p-input-block-->
<!--      v-model="value"-->
<!--      label="test label"-->
<!--      :autosize="true"-->
<!--    />-->
    <p-input-block
      v-model="value"
      label="test label"
      :autosize="true"
      :mask="formatNumeral"
    />
    <p-input-block
      v-model="textValue"
      :label="textValue"
      :autosize="true"
    />
    <p-input-block
      v-model="textValue"
      :label="textValue"
      :autosize="true"
      tag="textarea"
    />
  </story-block-wrapper>`,
  data () {
    return {
      value: '123456',
      textValue: 'text-value',
      formatNumeral: formatNumeral
    }
  }
}))
