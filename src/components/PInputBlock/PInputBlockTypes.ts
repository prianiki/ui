export type ForceErrorType = {
  active : boolean
  text : string
}

export type ValueType = boolean | string | number | null

export type ValidatorFunction = (value? : ValueType) => boolean
