import { storiesOf } from '@storybook/vue'

import PForm from './PForm'
import PInputBlock from '@/components/PInputBlock/PInputBlock.vue'

import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PForm', module).add('default', () => ({
  components: { PForm, StoryBlockWrapper, PInputBlock },
  template: `<story-block-wrapper>
    <p-form v-model="isValid">
      <p-input-block
        v-model="value"
      />
    </p-form>
  </story-block-wrapper>`,
  data () {
    return {
      value: 'test',
      isValid: false
    }
  }
}))
