import PInputBlock from '@/components/PInputBlock/PInputBlock.vue'

export type InputWatcherType = {
  _uid : number
  valid : Function
}

export type InputStatusesType = {
  [key: number] : boolean
}

export interface PInputBlockType extends PInputBlock {
  _uid : number
  reset : () => void
  onBlur : () => void
}
