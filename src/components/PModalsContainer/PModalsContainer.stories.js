import { storiesOf } from '@storybook/vue'

import PModalsContainer from './PModalsContainer'
import PModal from './PModal'
import StoryBlockWrapper from './../StoryBlockWrapper'

storiesOf('PModalsContainer', module)
  .add('default', () => ({
    components: { PModalsContainer, StoryBlockWrapper, PModal },
    template: `<story-block-wrapper>
      <p-modals-container>
        <div @click="isOpen = !isOpen">open/close</div>
        <div style="height: 1000px; width: 100%; background: pink"></div>
        <p-modal :value="isOpen">
          <div style="background: gray">modal inside</div>
        </p-modal>
      </p-modals-container>
    </story-block-wrapper>`,
    data () {
      return {
        isOpen: false
      }
    }
  }))
