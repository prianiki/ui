import { shallowMount } from '@vue/test-utils'
import PModalsContainer from '../PModalsContainer.vue'
import PModal from '../PModal.vue'

describe('p modals container', () => {
  const wrapper = shallowMount(PModalsContainer)

  it('should watch modal statuses', () => {
    wrapper.setData({
      modalStatuses: {
        valid: true
      }
    })

    // @ts-ignore
    expect(wrapper.vm.hasOpenedModals).toBeTruthy()
  })
})

describe('p modal', () => {
  const wrapper = shallowMount(PModal, {
    provide: {
      providedModals: {
        register () {
          return 'register'
        },
        unregister () {
          return 'unregister'
        }
      }
    }
  })

  it('should call provided methods', () => {
    // @ts-ignore
    expect(wrapper.vm.providedModals.register()).toEqual('register')
    // @ts-ignore
    expect(wrapper.vm.providedModals.unregister()).toEqual('unregister')
  })

  it('should emit input before destroy', () => {
    wrapper.destroy()

    expect(wrapper.emitted().input[0]).toEqual([false])
  })
})
