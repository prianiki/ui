import { Vue } from 'vue/types/vue'

export type ModalsStatusesType = {
  [key : number] : boolean
}

export type ModalType = Vue & {
  _uid : number
}

export type ModalWatcher = {
  _uid : number
  valid : Function
  shouldValidate : boolean | undefined
}
