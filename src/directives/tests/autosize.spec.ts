import autosize from '../autosize/index'

const { directive } = autosize

// const createHookArguments = (el = document.createElement('div'), binding = {}) => {
//   return [
//     el,
//     {
//       ...{
//         value: {
//           handler: () => jest.fn(),
//           events: ['dblclick'],
//           middleware: () => jest.fn(),
//           isActive: undefined
//         }
//       },
//       ...binding
//     }
//   ]
// }

describe('autosize plugin', () => {
  it('install the directive into vue instance', () => {
    const vue = {
      directive: jest.fn()
    }

    autosize.install(vue)
    expect(vue.directive).toHaveBeenCalledWith('autosize', autosize.directive)
    expect(vue.directive).toHaveBeenCalledTimes(1)
  })
})

describe('autosize directive', () => {
  it('it has bind, update and unbind methods available', () => {
    expect(typeof autosize.directive.bind).toBe('function')
    expect(typeof autosize.directive.componentUpdated).toBe('function')
    expect(typeof autosize.directive.unbind).toBe('function')
  })
})
