import directive from './autosize'

const plugin = {
  install (Vue : any) {
    Vue.directive('autosize', directive)
  },
  directive
}

export default plugin
