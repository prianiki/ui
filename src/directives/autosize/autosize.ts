import Vue, { DirectiveOptions } from 'vue'
import { DirectiveBinding } from 'vue/types/options'
import autosize from '@/utils/directives/autosize'
import autoSizeInput from '@/utils/directives/autosize-input'

const directive: DirectiveOptions = {
  bind: function (el, binding) {
    // @ts-ignore
    resize(el, binding)
  },
  componentUpdated: function (el, binding) {
    // @ts-ignore
    resize(el, binding)
  },
  unbind (el) {
    // autosize.destroy(el)
  }
}

const resize = (el : HTMLInputElement, binding : DirectiveBinding) => {
  const tagName = el.tagName
  if (binding.value !== true) return
  Vue.nextTick(() => {
    if (tagName === 'TEXTAREA') {
      autoSizeInput(el, { add: 10 })
      Vue.nextTick(() => {
        // @ts-ignore
        autosize(el)
      })
    } else if (tagName === 'INPUT' && el.type === 'text') autoSizeInput(el, { add: 5 })
  })
}

export default directive
